package com.rtucloud.cs.proxy.server;

import java.net.InetSocketAddress;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.rtucloud.cs.proxy.config.AppConfig;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

@Component
public class ExecutorsServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorsServer.class);

    @Autowired
    FrontendPipeline frontendPipeline;
    @Autowired
    AppConfig appConfig;

    @Async("frontendWorkTaskExecutor")
    public Future<Boolean> initProxyServer() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap boss = new ServerBootstrap();
            boss.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(frontendPipeline)
                    .childOption(ChannelOption.AUTO_READ, false);
            // 绑定端口
            ChannelFuture frontedChannel = boss.bind("localhost", 9191).sync();
            LOGGER.debug("启动代理服务,端口:" + ((InetSocketAddress) frontedChannel.channel().localAddress()).getPort());
            frontedChannel.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            LOGGER.debug("代理服务关闭!");
        } catch (Exception e) {
            LOGGER.error("代理服务启动失败!", e);
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
        return new AsyncResult<>(true);
    }

}
