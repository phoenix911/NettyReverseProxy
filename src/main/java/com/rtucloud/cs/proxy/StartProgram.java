package com.rtucloud.cs.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;


@SpringBootApplication
@EnableAdminServer
public class StartProgram {

    public static void main(String[] args) {
        SpringApplication.run(StartProgram.class, args);
    }


}
